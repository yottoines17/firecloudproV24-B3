resource scaleway_vpc_private_network "myvpc" {
}

resource scaleway_vpc_public_gateway_ip "pgw_ip" {
}

resource scaleway_vpc_public_gateway "pgw" {
  type = "VPC-GW-S"
  bastion_enabled = true
  ip_id = scaleway_vpc_public_gateway_ip.pgw_ip.id
}

resource scaleway_vpc_public_gateway_dhcp "dhcp" {
  subnet = "192.168.42.0/24"
  dns_local_name = scaleway_vpc_private_network.myvpc.name
  pool_low = "192.168.42.100"
  pool_high  = "192.168.42.200"
}
# Réservation DHCP pour le backend1
resource scaleway_vpc_public_gateway_dhcp_reservation backend1 {
    gateway_network_id = scaleway_vpc_gateway_network.mygw.id
    #mac_address = scaleway_instance_server.web1.private_network.1.mac_address
    #mac_address = scaleway_instance_private_nic.nic1.mac_address
    mac_address = scaleway_instance_server.backend1.private_network[0].mac_address

    ip_address = "192.168.42.101"
}
# Réservation DHCP pour le backend2
resource scaleway_vpc_public_gateway_dhcp_reservation backend2 {
    gateway_network_id = scaleway_vpc_gateway_network.mygw.id
    mac_address = scaleway_instance_server.backend2.private_network[0].mac_address

    ip_address = "192.168.42.102"
}

# Réservation DHCP pour le serveur de base de données
resource "scaleway_vpc_public_gateway_dhcp_reservation" "firecloud_bdd" {
  gateway_network_id = scaleway_vpc_gateway_network.mygw.id
  mac_address = scaleway_instance_server.firecloud_bdd.private_network[0].mac_address
  ip_address = "192.168.42.103"
}




resource scaleway_vpc_gateway_network "mygw" {
  gateway_id          = scaleway_vpc_public_gateway.pgw.id
  private_network_id  = scaleway_vpc_private_network.myvpc.id
  dhcp_id             = scaleway_vpc_public_gateway_dhcp.dhcp.id
  enable_dhcp         = true
}

# PAT rules

#resource "scaleway_vpc_public_gateway_pat_rule" "http" {
#  gateway_id = scaleway_vpc_public_gateway.pgw.id
#  #private_ip = scaleway_vpc_public_gateway_dhcp.dhcp.address
#  private_ip = "192.168.42.11"
#  private_port = 80
#  public_port = 80
#  protocol = "tcp"
#  depends_on = [scaleway_vpc_gateway_network.mygw, scaleway_vpc_private_network.myvpc]
#}
#
#resource "scaleway_vpc_public_gateway_pat_rule" "https" {
#  gateway_id = scaleway_vpc_public_gateway.pgw.id
#  #private_ip = scaleway_vpc_public_gateway_dhcp.dhcp.address
#  private_ip = "192.168.42.11" 
#  private_port = 443
#  public_port = 443
#  protocol = "tcp"
#  depends_on = [scaleway_vpc_gateway_network.mygw, scaleway_vpc_private_network.myvpc]
#}

# Output
# scaleway_vpc_public_gateway_ip.pgw_ip.address

output "bastion_ip" {
  value = "${scaleway_vpc_public_gateway_ip.pgw_ip.address}"
}

