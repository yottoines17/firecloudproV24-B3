#1ere instance contenant le backend1
resource "scaleway_instance_server" "backend1" {
    name  = "${local.team}-backend1"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "${local.team}", "web", "apache2","drupal1" ]
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/files/installdrupal")
    }
    enable_ipv6 = false
    private_network {
       pn_id = scaleway_vpc_private_network.myvpc.id
    }
}

# Seconde instance contenant le backend2

resource "scaleway_instance_server" "backend2" {
    name  = "${local.team}-backend2"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "${local.team}", "web", "apache2","drupal2" ]
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/files/installdrupal")
    }
    enable_ipv6 = false
    private_network {
       pn_id = scaleway_vpc_private_network.myvpc.id
    }
}
#3eme instance maria-db
resource "scaleway_instance_server" "firecloud_bdd" {
    name  = "firecloud_bdd"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = ["${local.team}","bdd", "mariadb" ]
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "db"
       cloud-init = file("${path.module}/files/installdb")
    }
    enable_ipv6 = false
        private_network {
        pn_id = scaleway_vpc_private_network.myvpc.id
    }

  }



# Données en sortie

output "backend1_private_ip" {
  value = "${scaleway_instance_server.backend1.private_ip}"
}

output "backend2_private_ip" {
  value = "${scaleway_instance_server.backend2.private_ip}"
}

output "bdd_public_ip" {
  value = "${scaleway_instance_server.firecloud_bdd.public_ip}"
}
