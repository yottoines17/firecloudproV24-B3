
resource "scaleway_instance_ip" "firecloud_srv1" {}

resource "scaleway_instance_server" "firecloud_srv1" {
    name  = "firecloud_srv1"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "web", "apache2" ]
    ip_id = scaleway_instance_ip.firecloud_srv1.id
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/files/installdrupal")
    }
  }

output "srv1_public_ip" {
  value = "${scaleway_instance_server.firecloud_srv1.public_ip}"
}

