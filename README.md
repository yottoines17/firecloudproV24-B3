# FirecloudproV24-B3


**Evaluation  Cours de Cloud Versailles  B3-2024 ;**

_**Notre groupe est fireCloudproV24-B3 ;**_

Le but de ce projet est de mettre à profit les compétences acquises en cours pour automatiser le déploiement d'une instance ou de plusieurs à l'aide de Tofu ;

**Déployer Drupal sur une seule instance** :

**1er Dossier  Tf_instance1drupal**

Ce dossier contient un repertoire files contenant un  fichier drupal.conf et un fichier installdrupal.

- Le fichier **drupal.conf** est utilisé pour configurer un hôte virtuel Apache pour servir un site web Drupal:
 On y indique à Apache d'écouter toutes les requêtes entrantes sur le port 80 (le port par défaut pour le trafic HTTP),  on y spécifie le répertoire racine du site web Drupal puis on autorise l'utilisation de fichiers .htaccess pour remplacer les directives de configuration Apache ;

- Le fichier automatise le déploiement d'un site web Drupal sur un serveur Ubuntu, en installant et configurant toutes les dépendances nécessaires ; Les étapes sont expliquées dans le fichier de deploiement ; 

On récupère le script go du cours pour  simplifier la connexion ssh aux instances ;

Le fichier provider.tf

 `terraform {
    required_providers {
      scaleway = {
        source = "scaleway/scaleway"
      }
    }
    required_version = ">= 0.13"
  }
` spécifie scaleway comme étant le service cloud sur lequel nos opérations sont réalisées ;

Enfin le fichier instance.tf decrit l'instance web apache à créer sur scaleway qui contient drupal ; (Au niveau de ce fichier changer firecloud par une expression qui decrit votre groupe etc)

**Consignes pour tester  le deploiement **

- Créer un dossier dans votre explorateur de fichiers ;
- Récupérez notre dossier Tf_instance1drupal sur  ce git  ;
- Modifier au niveau d'instance tf "firecloud" par le nom de votre équipe ou par le votre ;
-S'assurer d'avoir Terraform/ OpenTofu d'installé ;

On va donc utiliser OpenTofu 
Les commandes de base  pour rappel de Tofu/Terraform sont :

```
tofu init : installe le connecteur de votre fournisseur de cloud

tofu plan : valide et résume de déploiement

tofu apply : applique le déploiement (en cas de modification/ajout/suppression de ressource le déploiement est modifié

tofu output : montre les ressources déclarées comme telles (typiquement : adresses IP publiques inconnues à l'avance)

tofu destroy : détruit toutes les ressources créées
```
Du coup faire un tofu apply pour tester le deploiement ;

**Résultat du deploiement**

Ceci est censé deployer sur scaleway un serveur web apache2 dans lequel est installé drupal;
En output on a l'ip du serveur qui est renvoyé dans le terminal ;
Et le Blog Drupal est accessible sur l'url http://ip_de_votre_srv

**Le plan tofu à fournir**



```
OpenTofu used the selected providers to generate the following execution plan.
Resource actions are indicated with the following symbols:
  + create

OpenTofu will perform the following actions:

  # scaleway_instance_ip.firecloud_srv1 will be created
  + resource "scaleway_instance_ip" "firecloud_srv1" {
      + address         = (known after apply)
      + id              = (known after apply)
      + organization_id = (known after apply)
      + prefix          = (known after apply)
      + project_id      = (known after apply)
      + reverse         = (known after apply)
      + server_id       = (known after apply)
      + type            = (known after apply)
      + zone            = (known after apply)
    }

  # scaleway_instance_server.firecloud_srv1 will be created
  + resource "scaleway_instance_server" "firecloud_srv1" {
      + boot_type                        = "local"
      + bootscript_id                    = (known after apply)
      + cloud_init                       = (known after apply)
      + enable_dynamic_ip                = false
      + enable_ipv6                      = false
      + id                               = (known after apply)
      + image                            = "debian_bookworm"
      + ip_id                            = (known after apply)
      + ipv6_address                     = (known after apply)
      + ipv6_gateway                     = (known after apply)
      + ipv6_prefix_length               = (known after apply)
      + name                             = "firecloud_srv1"
      + organization_id                  = (known after apply)
      + placement_group_policy_respected = (known after apply)
      + private_ip                       = (known after apply)
      + project_id                       = (known after apply)
      + public_ip                        = (known after apply)
      + replace_on_type_change           = false
      + routed_ip_enabled                = (known after apply)
      + security_group_id                = (known after apply)
      + state                            = "started"
      + tags                             = [
          + "web",
          + "apache2",
        ]
      + type                             = "PRO2-XXS"
      + user_data                        = {
          + "cloud-init" = <<-EOT
                #!/usr/bin/env bash
                
                # Variables
                phpinifile="/etc/php/8.2/apache2/php.ini"
                drupaldirectory="/var/www/html/drupal"
                confdir="/root/firecloudproV24-B3/Evaluationtest3/Tf_instance1drupal/files"
                
                # Informations de la base de données
                db_name="drupal_db"
                db_user="drupal_user"
                db_password="password"
                
                # Mise à jour du système
                apt -y upgrade && apt -y update
                
                # Clonage du dépôt Git
                cd /root
                apt -y install git
                git clone https://gitlab.com/yottoines17/firecloudproV24-B3.git
                
                # Installation des outils requis
                apt -y install unzip wget 
                
                # Installation d'Apache2
                apt -y install apache2
                
                # Installation de PHP et de ses extensions
                apt -y install php libapache2-mod-php php-curl php-gd php-mbstring php-xml php-zip
                
                # Redémarrage d'Apache2
                systemctl restart apache2
                
                # Configuration de PHP et installation des dépendances nécessaires
                sed -i -e 's/max_execution_time =.*/max_execution_time = 300/' "$phpinifile"
                sed -i -e 's/memory_limit =.*/memory_limit = 512M/' "$phpinifile"
                sed -i -e 's/post_max_size =.*/post_max_size = 128M/' "$phpinifile"
                sed -i -e 's/upload_max_filesize =.*/upload_max_filesize = 128M/' "$phpinifile"
                
                apt -y install php php-curl php-fpm php-bcmath php-gd php-soap \
                	    php-zip php-curl php-mbstring php-mysqlnd php-gd \
                	    php-xml php-intl php-zip
                
                systemctl restart php8.2-fpm
                
                public_ip=$( curl -4 ifconfig.me )
                
                sed -i -e "s/PUBLIC_IP/$public_ip/" $confdir/drupal.conf
                
                
                # Installation de la base de données MariaDB
                apt -y install mariadb-server mariadb-client
                mysql_secure_installation<<EOT
                rootpw
                Y
                N
                Y
                Y
                Y
                EOT
                
                systemctl restart mariadb
                
                # Téléchargement et extraction de Drupal
                wget https://ftp.drupal.org/files/projects/drupal-9.4.0.tar.gz
                tar -xzvf drupal-9.4.0.tar.gz -C /var/www/html/
                mv "/var/www/html/drupal-9.4.0" "$drupaldirectory"
                
                # Configuration d'Apache2
                cp "$confdir/drupal.conf" /etc/apache2/sites-available/
                a2ensite drupal.conf
                a2dissite 000-default
                systemctl restart apache2
                
                # Configuration de la base de données
                mysql -u root -prootpw <<EOT
                CREATE DATABASE $db_name;
                GRANT ALL PRIVILEGES ON $db_name.* TO '$db_user'@'localhost' IDENTIFIED BY '$db_password';
                FLUSH PRIVILEGES;
                EXIT
                EOT
                
                # Configuration de Drupal
                cp "$confdir/sites/default/default.settings.php" "$drupaldirectory/sites/default/settings.php"
                
                # Ajout de la configuration de la base de données à settings.php
                cat <<EOL >> "$drupaldirectory/sites/default/settings.php"
                \$databases['default']['default'] = array(
                  'database' => '$db_name',
                  'username' => '$db_user',
                  'password' => '$db_password',
                  'prefix' => '',
                  'host' => 'localhost',
                  'port' => '3306',
                  'namespace' => 'Drupal\\\\Core\\\\Database\\\\Driver\\\\mysql',
                  'driver' => 'mysql',
                );
                EOL
                
                # Création du répertoire des fichiers et ajustement des permissions
                mkdir "$drupaldirectory/sites/default/files"
                chown -R www-data:www-data "$drupaldirectory/sites/default/files"
                
                # Installation de Drupal terminée
                touch /tmp/drupalok
            EOT
          + "role"       = "web"
        }
      + zone                             = (known after apply)

      + root_volume {
          + boot                  = false
          + delete_on_termination = true
          + name                  = (known after apply)
          + size_in_gb            = 10
          + volume_id             = (known after apply)
          + volume_type           = (known after apply)
        }
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + srv1_public_ip = (known after apply)
```




**Déployer Drupal sur une plusieurs instances ** :

**1er Dossier  Tf_instance3drupal**


**Schéma de l'architecture **

Le schéma est disponible dans le dossier soustitré schéma ;


La structure se resume comme suit :

On effectue le deploiement de deux backends drupal sur  deux instances de scaleway , chacun contenant un serveur apache et drupal ; Ils sont reliés et communiquent avec une base de donnée installée sur une instance à part ; Le tout dans un vpc avec un bastion et un load balancer  ;

On utilise le même repertoire files contenant les mêmes fichiers installdrupal et drupal.conf mais aussi un fichier installdb qui automatise l'installation de la base de données sur une instance ;


- Le Makefile permet  d'automatiser le processus de déploiement et de gestion avec  les commandes tofu  ;
- le fichier instance.tf définit trois ressources d'instances Scaleway ( les ressources Backend 1 et 2 qui installent à partir du cloud init drupal via  le fichier installdrupal , une ressource firecloud_bdd qui installe mariadb via le fichier installdb )
-Le fichier lb.tf deploie un frontend et un backend pour le load balancer ;
- Le fichier vpc.tf decrit les addresses ip de chaque instance et les caractéristiques du réseau ;
- Le fichier teamname.txt est à modifié et contient un nom à personnaliser ;

**Consignes pour tester  le deploiement **

- Créer un dossier dans votre explorateur de fichiers ;
- Récupérez notre dossier Tf_instance3drupal sur  ce git  ;
- Modifier au niveau du teamname.txt et mettre le nom de votre equipe ;
-S'assurer d'avoir Terraform/ OpenTofu d'installé ;
-Run tofu apply pour appliquer le deploiement 

**Résultat du deploiement**

Ceci est censé deployer sur scaleway un système de trois instances : deux backend drupal et une base de données maria db. Le tout dans un vpc avec un load balancer et un bastion qui permet la connexion aux principales instances;
En output on a les trois ip des serveurs qui sont renvoyés dans le terminal ;
Au bout de quelques minutes on peut tester que la configuration fonctionne : l'url http://lb_ip/

**Le plan tofu à fournir**

OpenTofu used the selected providers to generate the following execution plan.
Resource actions are indicated with the following symbols:
  + create

OpenTofu will perform the following actions:

  # scaleway_instance_server.backend1 will be created
  + resource "scaleway_instance_server" "backend1" {
      + boot_type                        = "local"
      + bootscript_id                    = (known after apply)
      + cloud_init                       = (known after apply)
      + enable_dynamic_ip                = false
      + enable_ipv6                      = false
      + id                               = (known after apply)
      + image                            = "debian_bookworm"
      + ipv6_address                     = (known after apply)
      + ipv6_gateway                     = (known after apply)
      + ipv6_prefix_length               = (known after apply)
      + name                             = "firecloud-backend1"
      + organization_id                  = (known after apply)
      + placement_group_policy_respected = (known after apply)
      + private_ip                       = (known after apply)
      + project_id                       = (known after apply)
      + public_ip                        = (known after apply)
      + replace_on_type_change           = false
      + routed_ip_enabled                = (known after apply)
      + security_group_id                = (known after apply)
      + state                            = "started"
      + tags                             = [
          + "firecloud",
          + "web",
          + "apache2",
          + "drupal1",
        ]
      + type                             = "PRO2-XXS"
      + user_data                        = {
          + "cloud-init" = <<-EOT
                #!/usr/bin/env bash
                
                # Variables
                phpinifile="/etc/php/8.2/apache2/php.ini"
                drupaldirectory="/var/www/html/drupal"
                confdir="/root/firecloudproV24-B3/Evaluationtest3/Tf_instance3drupal/files"
                
                # Informations de la base de données
                db_name="drupal_db"
                db_user="drupal_user"
                db_password="password"
                
                
                # Mise à jour du système
                apt -y upgrade && apt -y update
                
                # Clonage du dépôt Git
                cd /root
                apt -y install git
                git clone https://gitlab.com/yottoines17/firecloudproV24-B3.git
                
                # Installation des outils requis
                apt -y install unzip wget 
                
                # Installation d'Apache2
                apt -y install apache2
                
                # Installation de PHP et de ses extensions
                apt -y install php libapache2-mod-php php-curl php-gd php-mbstring php-xml php-zip
                
                # Redémarrage d'Apache2
                systemctl restart apache2
                
                # Configuration de PHP et installation des dépendances nécessaires
                sed -i -e 's/max_execution_time =.*/max_execution_time = 300/' "$phpinifile"
                sed -i -e 's/memory_limit =.*/memory_limit = 512M/' "$phpinifile"
                sed -i -e 's/post_max_size =.*/post_max_size = 128M/' "$phpinifile"
                sed -i -e 's/upload_max_filesize =.*/upload_max_filesize = 128M/' "$phpinifile"
                
                apt -y install php php-curl php-fpm php-bcmath php-gd php-soap \
                    php-zip php-curl php-mbstring php-mysqlnd php-gd \
                    php-xml php-intl php-zip
                
                systemctl restart php8.2-fpm
                
                public_ip=$(curl -4 ifconfig.me)
                
                sed -i -e "s/PUBLIC_IP/$public_ip/" "$confdir/drupal.conf"
                
                # Téléchargement et extraction de Drupal
                wget https://ftp.drupal.org/files/projects/drupal-9.4.0.tar.gz
                tar -xzvf drupal-9.4.0.tar.gz -C /var/www/html/
                mv "/var/www/html/drupal-9.4.0" "$drupaldirectory"
                
                # Configuration d'Apache2
                cp "$confdir/drupal.conf" /etc/apache2/sites-available/
                a2ensite drupal.conf
                a2dissite 000-default
                systemctl restart apache2
                
                # Configuration de Drupal
                cp "$confdir/sites/default/default.settings.php" "$drupaldirectory/sites/default/settings.php"
                
                # Ajout de la configuration de la base de données à settings.php
                cat <<EOL >> "$drupaldirectory/sites/default/settings.php"
                \$databases['default']['default'] = array(
                  'database' => '$db_name',
                  'username' => '$db_user',
                  'password' => '$db_password',
                  'prefix' => '',
                  'host' => 'localhost',
                  'port' => '3306',
                  'namespace' => 'Drupal\\\\Core\\\\Database\\\\Driver\\\\mysql',
                  'driver' => 'mysql',
                );
                EOL
                # Création du répertoire des fichiers et ajustement des permissions
                mkdir "$drupaldirectory/sites/default/files"
                chown -R www-data:www-data "$drupaldirectory/sites/default/files"
                systemctl restart apache2
                
                # Installation de Drupal terminée
                touch /tmp/drupalok
            EOT
          + "role"       = "web"
        }
      + zone                             = (known after apply)

      + private_network {
          + mac_address = (known after apply)
          + pn_id       = (known after apply)
          + status      = (known after apply)
          + zone        = (known after apply)
        }

      + root_volume {
          + boot                  = false
          + delete_on_termination = true
          + name                  = (known after apply)
          + size_in_gb            = 10
          + volume_id             = (known after apply)
          + volume_type           = (known after apply)
        }
    }

  # scaleway_instance_server.backend2 will be created
  + resource "scaleway_instance_server" "backend2" {
      + boot_type                        = "local"
      + bootscript_id                    = (known after apply)
      + cloud_init                       = (known after apply)
      + enable_dynamic_ip                = false
      + enable_ipv6                      = false
      + id                               = (known after apply)
      + image                            = "debian_bookworm"
      + ipv6_address                     = (known after apply)
      + ipv6_gateway                     = (known after apply)
      + ipv6_prefix_length               = (known after apply)
      + name                             = "firecloud-backend2"
      + organization_id                  = (known after apply)
      + placement_group_policy_respected = (known after apply)
      + private_ip                       = (known after apply)
      + project_id                       = (known after apply)
      + public_ip                        = (known after apply)
      + replace_on_type_change           = false
      + routed_ip_enabled                = (known after apply)
      + security_group_id                = (known after apply)
      + state                            = "started"
      + tags                             = [
          + "firecloud",
          + "web",
          + "apache2",
          + "drupal2",
        ]
      + type                             = "PRO2-XXS"
      + user_data                        = {
          + "cloud-init" = <<-EOT
                #!/usr/bin/env bash
                
                # Variables
                phpinifile="/etc/php/8.2/apache2/php.ini"
                drupaldirectory="/var/www/html/drupal"
                confdir="/root/firecloudproV24-B3/Evaluationtest3/Tf_instance3drupal/files"
                
                # Informations de la base de données
                db_name="drupal_db"
                db_user="drupal_user"
                db_password="password"
                
                
                # Mise à jour du système
                apt -y upgrade && apt -y update
                
                # Clonage du dépôt Git
                cd /root
                apt -y install git
                git clone https://gitlab.com/yottoines17/firecloudproV24-B3.git
                
                # Installation des outils requis
                apt -y install unzip wget 
                
                # Installation d'Apache2
                apt -y install apache2
                
                # Installation de PHP et de ses extensions
                apt -y install php libapache2-mod-php php-curl php-gd php-mbstring php-xml php-zip
                
                # Redémarrage d'Apache2
                systemctl restart apache2
                
                # Configuration de PHP et installation des dépendances nécessaires
                sed -i -e 's/max_execution_time =.*/max_execution_time = 300/' "$phpinifile"
                sed -i -e 's/memory_limit =.*/memory_limit = 512M/' "$phpinifile"
                sed -i -e 's/post_max_size =.*/post_max_size = 128M/' "$phpinifile"
                sed -i -e 's/upload_max_filesize =.*/upload_max_filesize = 128M/' "$phpinifile"
                
                apt -y install php php-curl php-fpm php-bcmath php-gd php-soap \
                    php-zip php-curl php-mbstring php-mysqlnd php-gd \
                    php-xml php-intl php-zip
                
                systemctl restart php8.2-fpm
                
                public_ip=$(curl -4 ifconfig.me)
                
                sed -i -e "s/PUBLIC_IP/$public_ip/" "$confdir/drupal.conf"
                
                # Téléchargement et extraction de Drupal
                wget https://ftp.drupal.org/files/projects/drupal-9.4.0.tar.gz
                tar -xzvf drupal-9.4.0.tar.gz -C /var/www/html/
                mv "/var/www/html/drupal-9.4.0" "$drupaldirectory"
                
                # Configuration d'Apache2
                cp "$confdir/drupal.conf" /etc/apache2/sites-available/
                a2ensite drupal.conf
                a2dissite 000-default
                systemctl restart apache2
                
                # Configuration de Drupal
                cp "$confdir/sites/default/default.settings.php" "$drupaldirectory/sites/default/settings.php"
                
                # Ajout de la configuration de la base de données à settings.php
                cat <<EOL >> "$drupaldirectory/sites/default/settings.php"
                \$databases['default']['default'] = array(
                  'database' => '$db_name',
                  'username' => '$db_user',
                  'password' => '$db_password',
                  'prefix' => '',
                  'host' => 'localhost',
                  'port' => '3306',
                  'namespace' => 'Drupal\\\\Core\\\\Database\\\\Driver\\\\mysql',
                  'driver' => 'mysql',
                );
                EOL
                # Création du répertoire des fichiers et ajustement des permissions
                mkdir "$drupaldirectory/sites/default/files"
                chown -R www-data:www-data "$drupaldirectory/sites/default/files"
                systemctl restart apache2
                
                # Installation de Drupal terminée
                touch /tmp/drupalok
            EOT
          + "role"       = "web"
        }
      + zone                             = (known after apply)

      + private_network {
          + mac_address = (known after apply)
          + pn_id       = (known after apply)
          + status      = (known after apply)
          + zone        = (known after apply)
        }

      + root_volume {
          + boot                  = false
          + delete_on_termination = true
          + name                  = (known after apply)
          + size_in_gb            = 10
          + volume_id             = (known after apply)
          + volume_type           = (known after apply)
        }
    }

  # scaleway_instance_server.firecloud_bdd will be created
  + resource "scaleway_instance_server" "firecloud_bdd" {
      + boot_type                        = "local"
      + bootscript_id                    = (known after apply)
      + cloud_init                       = (known after apply)
      + enable_dynamic_ip                = false
      + enable_ipv6                      = false
      + id                               = (known after apply)
      + image                            = "debian_bookworm"
      + ipv6_address                     = (known after apply)
      + ipv6_gateway                     = (known after apply)
      + ipv6_prefix_length               = (known after apply)
      + name                             = "firecloud_bdd"
      + organization_id                  = (known after apply)
      + placement_group_policy_respected = (known after apply)
      + private_ip                       = (known after apply)
      + project_id                       = (known after apply)
      + public_ip                        = (known after apply)
      + replace_on_type_change           = false
      + routed_ip_enabled                = (known after apply)
      + security_group_id                = (known after apply)
      + state                            = "started"
      + tags                             = [
          + "firecloud",
          + "bdd",
          + "mariadb",
        ]
      + type                             = "PRO2-XXS"
      + user_data                        = {
          + "cloud-init" = <<-EOT
                #!/usr/bin/env bash
                
                # Variables
                phpinifile="/etc/php/8.2/apache2/php.ini"
                drupaldirectory="/var/www/html/drupal"
                confdir="/root/firecloudproV24-B3/Evaluationtest3/Tf_instance3drupal/files"
                
                # Informations de la base de données
                db_name="drupal_db"
                db_user="drupal_user"
                db_password="password"
                
                # Mise à jour du système
                apt -y upgrade && apt -y update
                
                # Clonage du dépôt Git
                cd /root
                apt -y install git
                git clone https://gitlab.com/yottoines17/firecloudproV24-B3.git
                
                # Installation des outils requis
                apt -y install unzip wget 
                
                public_ip=$( curl -4 ifconfig.me )
                
                # Installation de la base de données MariaDB
                apt -y install mariadb-server mariadb-client
                mysql_secure_installation <<EOT
                rootpw
                Y
                N
                Y
                Y
                Y
                EOT
                
                systemctl restart mariadb
                
                mysql -u root -prootpw<<EOT
                CREATE DATABASE db_name;
                GRANT ALL PRIVILEGES ON db_name.*\
                  TO 'db_user'@'%' IDENTIFIED BY 'db_password';
                FLUSH PRIVILEGES;
                EXIT
                EOT
                
                sed -i -e "s/^#.*port *3306/port 3306/" /etc/mysql/my.cnf
                sed -i -e "s/^bind-address.*/bind-address */" /etc/mysql/mariadb.conf.d/50-server
                
                touch /tmp/dpdbok
            EOT
          + "role"       = "db"
        }
      + zone                             = (known after apply)

      + private_network {
          + mac_address = (known after apply)
          + pn_id       = (known after apply)
          + status      = (known after apply)
          + zone        = (known after apply)
        }

      + root_volume {
          + boot                  = false
          + delete_on_termination = true
          + name                  = (known after apply)
          + size_in_gb            = 10
          + volume_id             = (known after apply)
          + volume_type           = (known after apply)
        }
    }

  # scaleway_lb.lb will be created
  + resource "scaleway_lb" "lb" {
      + id                      = (known after apply)
      + ip_address              = (known after apply)
      + ip_id                   = (known after apply)
      + name                    = "firecloud"
      + organization_id         = (known after apply)
      + project_id              = (known after apply)
      + region                  = (known after apply)
      + ssl_compatibility_level = "ssl_compatibility_level_intermediate"
      + type                    = "LB-S"
      + zone                    = (known after apply)

      + private_network {
          + dhcp_config        = true
          + private_network_id = (known after apply)
          + static_config      = []
          + status             = (known after apply)
          + zone               = (known after apply)
        }
    }

  # scaleway_lb_backend.backend will be created
  + resource "scaleway_lb_backend" "backend" {
      + forward_port                 = 80
      + forward_port_algorithm       = "roundrobin"
      + forward_protocol             = "http"
      + health_check_delay           = "60s"
      + health_check_max_retries     = 2
      + health_check_port            = (known after apply)
      + health_check_send_proxy      = false
      + health_check_timeout         = "30s"
      + health_check_transient_delay = "0.5s"
      + id                           = (known after apply)
      + ignore_ssl_server_verify     = false
      + lb_id                        = (known after apply)
      + max_retries                  = 3
      + name                         = "backend"
      + on_marked_down_action        = "none"
      + proxy_protocol               = "none"
      + send_proxy_v2                = (known after apply)
      + server_ips                   = [
          + "192.168.42.101",
          + "192.168.42.102",
        ]
      + ssl_bridging                 = false
      + sticky_sessions              = "none"
      + timeout_connect              = "5s"
      + timeout_queue                = "0s"
      + timeout_server               = "5m"
      + timeout_tunnel               = "15m"
    }

  # scaleway_lb_frontend.frontend will be created
  + resource "scaleway_lb_frontend" "frontend" {
      + backend_id     = (known after apply)
      + certificate_id = (known after apply)
      + enable_http3   = false
      + external_acls  = false
      + id             = (known after apply)
      + inbound_port   = 80
      + lb_id          = (known after apply)
      + name           = "frontend"
    }

  # scaleway_lb_ip.lb will be created
  + resource "scaleway_lb_ip" "lb" {
      + id              = (known after apply)
      + ip_address      = (known after apply)
      + lb_id           = (known after apply)
      + organization_id = (known after apply)
      + project_id      = (known after apply)
      + region          = (known after apply)
      + reverse         = (known after apply)
      + zone            = (known after apply)
    }

  # scaleway_vpc_gateway_network.mygw will be created
  + resource "scaleway_vpc_gateway_network" "mygw" {
      + cleanup_dhcp       = false
      + created_at         = (known after apply)
      + dhcp_id            = (known after apply)
      + enable_dhcp        = true
      + enable_masquerade  = true
      + gateway_id         = (known after apply)
      + id                 = (known after apply)
      + mac_address        = (known after apply)
      + private_network_id = (known after apply)
      + static_address     = (known after apply)
      + status             = (known after apply)
      + updated_at         = (known after apply)
      + zone               = (known after apply)
    }

  # scaleway_vpc_private_network.myvpc will be created
  + resource "scaleway_vpc_private_network" "myvpc" {
      + created_at      = (known after apply)
      + id              = (known after apply)
      + is_regional     = (known after apply)
      + name            = (known after apply)
      + organization_id = (known after apply)
      + project_id      = (known after apply)
      + region          = (known after apply)
      + updated_at      = (known after apply)
      + vpc_id          = (known after apply)
      + zone            = (known after apply)
    }

  # scaleway_vpc_public_gateway.pgw will be created
  + resource "scaleway_vpc_public_gateway" "pgw" {
      + bastion_enabled = true
      + bastion_port    = (known after apply)
      + created_at      = (known after apply)
      + enable_smtp     = (known after apply)
      + id              = (known after apply)
      + ip_id           = (known after apply)
      + name            = (known after apply)
      + organization_id = (known after apply)
      + project_id      = (known after apply)
      + status          = (known after apply)
      + type            = "VPC-GW-S"
      + updated_at      = (known after apply)
      + zone            = (known after apply)
    }

  # scaleway_vpc_public_gateway_dhcp.dhcp will be created
  + resource "scaleway_vpc_public_gateway_dhcp" "dhcp" {
      + address              = (known after apply)
      + created_at           = (known after apply)
      + dns_local_name       = (known after apply)
      + dns_search           = (known after apply)
      + dns_servers_override = (known after apply)
      + enable_dynamic       = (known after apply)
      + id                   = (known after apply)
      + organization_id      = (known after apply)
      + pool_high            = "192.168.42.200"
      + pool_low             = "192.168.42.100"
      + project_id           = (known after apply)
      + push_default_route   = (known after apply)
      + push_dns_server      = (known after apply)
      + rebind_timer         = (known after apply)
      + renew_timer          = (known after apply)
      + subnet               = "192.168.42.0/24"
      + updated_at           = (known after apply)
      + valid_lifetime       = (known after apply)
      + zone                 = (known after apply)
    }

  # scaleway_vpc_public_gateway_dhcp_reservation.backend1 will be created
  + resource "scaleway_vpc_public_gateway_dhcp_reservation" "backend1" {
      + created_at         = (known after apply)
      + gateway_network_id = (known after apply)
      + hostname           = (known after apply)
      + id                 = (known after apply)
      + ip_address         = "192.168.42.101"
      + mac_address        = (known after apply)
      + type               = (known after apply)
      + updated_at         = (known after apply)
      + zone               = (known after apply)
    }

  # scaleway_vpc_public_gateway_dhcp_reservation.backend2 will be created
  + resource "scaleway_vpc_public_gateway_dhcp_reservation" "backend2" {
      + created_at         = (known after apply)
      + gateway_network_id = (known after apply)
      + hostname           = (known after apply)
      + id                 = (known after apply)
      + ip_address         = "192.168.42.102"
      + mac_address        = (known after apply)
      + type               = (known after apply)
      + updated_at         = (known after apply)
      + zone               = (known after apply)
    }

  # scaleway_vpc_public_gateway_dhcp_reservation.firecloud_bdd will be created
  + resource "scaleway_vpc_public_gateway_dhcp_reservation" "firecloud_bdd" {
      + created_at         = (known after apply)
      + gateway_network_id = (known after apply)
      + hostname           = (known after apply)
      + id                 = (known after apply)
      + ip_address         = "192.168.42.103"
      + mac_address        = (known after apply)
      + type               = (known after apply)
      + updated_at         = (known after apply)
      + zone               = (known after apply)
    }

  # scaleway_vpc_public_gateway_ip.pgw_ip will be created
  + resource "scaleway_vpc_public_gateway_ip" "pgw_ip" {
      + address         = (known after apply)
      + created_at      = (known after apply)
      + id              = (known after apply)
      + organization_id = (known after apply)
      + project_id      = (known after apply)
      + reverse         = (known after apply)
      + updated_at      = (known after apply)
      + zone            = (known after apply)
    }

Plan: 15 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + backend1_private_ip = (known after apply)
  + backend2_private_ip = (known after apply)
  + bastion_ip          = (known after apply)
  + bdd_public_ip       = (known after apply)
  + lb_ip               = (known after apply)

───────────────────────────────────────────────────────────────────────────────











